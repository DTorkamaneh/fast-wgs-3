#!/bin/bash

#SBATCH -J WGS
#SBATCH -o wgs-%j.out
#SBATCH -c 10
#SBATCH -p small
#SBATCH --mail-type=ALL
#SBATCH --mail-user=@ibis.ulaval.ca
#SBATCH --time=1-00:00
#SBATCH --mem=14400

module load bwa/0.7.13
module load samtools/1.3
module load vcftools/0.1.12b
module load platypus/0.8.1

ulimit -S -n 40000

./fastwgs.sh parameters.txt

