# FastWGS

A bioinformatic pipeline for whole genome sequencing (WGS) files processing

## Introduction

FastWGS is a bash pipeline facilitating the processing of FASTQ sequence files obtained by whole genome sequencing (WGS). It includes a set of bash commands, python homemade scripts, and well known bioinformatics software such as bwa, samtools and platypus.
Users simply fill out a parameter file and then launch the program.
For each analyzed case it requires the existence of a reference genome, completed or not that is to say consisting of scaffolds and contigs.

The main difference with GBS resides in the de-multiplexing step which is absent from WGS. Also, in GBS single end sequences are usually used whereas in WGS that are paired sequences which are used.

## Glossary

This section should allow the user to better understand the terms used throughout the file preparation protocol.

This section should allow the user to better understand the terms used throughout the file preparation protocol.

FLOWCELL: Sequencing batch. Originally can contains up to 8 lanes

LANES: File containing multiplex sequences. Several lanes can be associated to a flowcell.

TECHNOLOGY: Sequencing technology, either Illumina or IonTorrent

## Using fastwgs

The main steps in using FastWGS are:

- Run the script `./makeDir.sh` that will create the following directories:  
	`refgenome`  
	`data`    
	`results` 
	`control_quality` 
	
- Prepare the reference genome
- Prepare the data
- Prepare the parameters file
- Run the script `fastwgs.sh`  

## Dependencies

In order to use fastwgs, you will need the following:

- Linux with parallel installed (http://www.gnu.org/software/parallel/)
- Python 2.7 or higher (https://www.python.org/)
- bwa (https://github.com/lh3/bwa)
- samtools (http://www.htslib.org/)
- platypus (http://www.well.ox.ac.uk/platypus)
- vcf python module (https://github.com/jamescasbon/PyVCF)
- fastwgs.sh (this distribution)
- fastwgs_parameters.txt (this distribution)
- make_directories.sh (this distribution)
- vcf2txt.py (this distribution)

## Additionnal softwares
This software is not directly in the pipeline but it is recommanded for quality check:  
- fastqc (http://www.bioinformatics.babraham.ac.uk/projects/fastqc/)

## Preparation the reference genome

Move your reference genome file in the `refgenome` directory, move to that directory and index it with the command:  
```bwa index -a bwtsw refgenome.fasta```

The .fai file isn't created as part of "bwa index."  To create it, run the command:  
```samtools faidx refgenome.fasta```

That will create a file named refgenome.fasta.fai.

Finally, write the reference genome file name in the parameter file:

`REFGEN=refgenome.fasta`

## Preparation the data

Move your sequence files in the `data` directory.  
For WGS, we use paired-ends sequence files and the samples are already demultiplexed.
Rename your sample files in that way:  
```
HN104_1.fq.gz  
HN104_2.fq.gz  
HN105_1.fq.gz  
HN105_2.fq.gz  
HN106_1.fq.gz  
HN106_2.fq.gz  
HN107_1.fq.gz  
HN107_2.fq.gz  
HN108_1.fq.gz  
HN108_2.fq.gz  
```
**It is important to keep the extension .fq.gz because for simplicity it is hardcoded in the pipeline.**

## Preparation the parameters file

Verify the information in the file `fastwgs_parameters.txt`.
If necessary, change the value given to the variables.
**It is very important not to change the words in capital letters (before the sign =). If you do this, you will get an error message because the pipeline will not find that variable.**  
Respect what is an integer and a string.

## Running fastwgs

To run the pipeline, just enter the command:
```./fastwgs.sh fastwgs_parameters.txt```

## Supplementary information

The parameter file allows the user to change the value of certain variables in the pipeline. However, the list of variables in the parameter file is far from exhaustive. It is therefore important to consider that several parameters of the software used in this pipeline, are hard-coded. We made this choice to make life easier for less experienced users.
In line with this philosophy, when the user chooses the ILLUMINA technology in the parameter file, values are automatically given to the following variables in Platypus:  
```
--genIndels=1  
--minMapQual=20  
--minBaseQual=20  
```  
When the IONTORRENT technology is chosen:  
```
--genIndels=0  
--minMapQual=10  
--minBaseQual=10  
```
These platypus options are hardcoded in the pipeline:  
```
--maxHaplotypes: 70
--originalMaxHaplotypes: 70
--minFlank: 3
--maxVariants: 10
--badReadsWindow: 5
--largeWindows: 1
--longHaps: 1
--filterReadPairsWithSmallInserts: 0
--minVarFreq: 0.002
--assembleBrokenPairs: 1
```
However, advanced users can easily edit the fastgbs.sh code and to put the values of their choice.

### Platypus  
A common problem that can occur with platypus is where the system says it can not find a
file (BAM) in the sample list to use. This problem is caused by the variable that defines
the number of files that can be opened simultaneously. To get the current value used by
the system, run the command `ulimit -a`. This value can be of 1024.  
To avoid this problem, we must consider the number of CPUs you give to platypus and the
number of samples (bam file) you want to analyze. The number of files that must be opened
simultaneously be: CPU X Nb Nb samples.  
The number of CPUs determine the number of genomic regions analyzed simultaneously.
For each analyzed genomic region, all bam files must be opened by the program. For example,
if you have 180 samples and you give 10 CPUs, this is 1800 files to be opened simultaneously.
If the limit of your OS is 1024, you will get an error message. In this case you can
give 1024/180 = 5.6, so 5 CPUs. If you can raise the limit 2048 (ulimit -n 2048) you can
give 2048/180 = 11.4, 11 CPUs.  
To change the limit, enter this command:  
`ulimit -S -n 5120`


### Quality control

We strongly recommand users to check for sequence quality with `fastqc` software.

## Citing

If you use fastwgs in your research, please cite:

## References

O. Tange (2011): GNU Parallel - The Command-Line Power Tool: The USENIX Magazine, February 2011:42-47.

## License

FastWGS is licensed under the GNU General Public Licence version 3 (GPL3). See
the LICENCE file for more details.
